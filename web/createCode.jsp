<%--
  Created by IntelliJ IDEA.
  User: loveshgrover
  Date: 12/12/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>PayU</title>
</head>
<body style="text-align: center">


<%
    //allow access only if session exists
    String user = null;
    if (session.getAttribute("user") == null) {
        response.sendRedirect("login");
    } else user = (String) session.getAttribute("user");
    String userName = null;
    String sessionID = null;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) userName = cookie.getValue();
            if (cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
        }
    }
%>

<div style="border: solid 1px;width:80%; display: inline-block;padding: 40px">
    <div style="float: left;width: 50%; text-align: left;">
        <form action="createCode" method="post">

            <table>
                <tr>
                    <td>
                        <select name="state" id="state" style="width: 150px">
                            <option>State*</option>
                            <c:forEach items="${states}" var="state">
                                <option>${state}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select name="aType" id="aType" style="width: 150px">
                            <option>A Type(C/S)*</option>
                            <option>C</option>
                            <option>S</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="number" placeholder="Ref No." name="refNo" style="width: 150px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <select name="dType" id="dType" style="width: 150px">
                            <option>D Type(G/P/U)*</option>
                            <option>G</option>
                            <option>P</option>
                            <option>U</option>
                        </select>
                    </td>
                </tr>
            </table>

            <input type="submit" title="Create Code" style="margin-left:30px;margin-top: 30px;margin-right: 30px">
            <input type="reset" title="Cancel">
            <br/>

        </form>
    </div>
    <div style="float: right;width: 50%; text-align: center;height:155px;">
        <br/>
        <br/>
        <strong><u>${unique}</u></strong>
    </div>
    <br/>
    ${code}
    ${errorDropDownEmpty}
    ${errorDropDownInvalid}
    ${errorRefNo}
</div>


<br/>

<br/>
</body>
</html>
