<%--
  Created by IntelliJ IDEA.
  User: loveshgrover
  Date: 13/12/18
  Time: 4:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<html>
<head>
    <title>Title</title>
    <script type="text/javascript">
        $(window).load(function () {

            function sortTable(f, n) {
                var rows = $('#mytable tbody  tr').get();

                rows.sort(function (a, b) {

                    var A = getVal(a);
                    var B = getVal(b);

                    if (A < B) {
                        return -1 * f;
                    }
                    if (A > B) {
                        return 1 * f;
                    }
                    return 0;
                });

                function getVal(elm) {
                    return new Date($(elm).children('td').eq(n).text().toUpperCase());
                }

                $.each(rows, function (index, row) {
                    $('#mytable').children('tbody').append(row);
                });
            }

            var f_sl = 1;
            var f_nm = 1;
            $("#sl").click(function () {
                f_sl *= -1;
                var n = $(this).prevAll().length;
                sortTable(f_sl, n);
            });
        });
        function searchOnTable(index, id) {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById(id);
            filter = input.value.toUpperCase();
            table = document.getElementById("mytable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[index];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        function exportToCSV() {
            let csvContent = "data:text/csv;charset=utf-8,";
            let row = "State,A Type,D Type,Code,Ref No";
            csvContent += row + "\r\n";
            <c:forEach items="${searchCodes}" var="searchCode">
            row = '${searchCode.state}' + ","
                    + '${searchCode.aType}' + ","
                    + '${searchCode.dType}' + ","
                    + '${searchCode.gCode}' + ","
                    + '${searchCode.refNo}';
            csvContent += row + "\r\n";
            row = "";
            </c:forEach>

            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", "exportToCsv.csv");
            document.body.appendChild(link); // Required for FF
            link.click();
        }


    </script>
</head>
<body style="text-align: center">


<%
    //allow access only if session exists
    String user = null;
    if (session.getAttribute("user") == null) {
        response.sendRedirect("login");
    } else user = (String) session.getAttribute("user");
    String userName = null;
    String sessionID = null;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) userName = cookie.getValue();
            if (cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
        }
    }
%>

<div style="display: inline-block;align-self: center">

    <form action="searchCode" method="post">
        <div style="text-align:left;">
            <input type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')"
                   name="fromDate"
                   placeholder="From Date">
            <input type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')" name="toDate"
                   placeholder="To Date">
            <input type="submit" title="Search">
        </div>
        </br>
        </br>
        <div style="text-align:right;"><a href="#foo" style='text-align: left' onclick=exportToCSV()>Export to Csv</a>
        </div>
        <br/>

        <table id="mytable" border="1"
               style="border-collapse: collapse;overflow-y:scroll;max-height:200px;display:inline-block">
            <thead style="background: #cecece">
            <tr>
                <th>
                    <input type="search" id="state" placeholder="State" onkeyup=searchOnTable(0,'state')>
                </th>
                <th><input type="search" placeholder="A Type" id="aType" onkeyup=searchOnTable(1,'aType')></th>
                <th>
                    <input type="search" placeholder="D Type" id="dType" onkeyup=searchOnTable(2,'dType')>
                </th>
                <th>
                    <input type="search" placeholder="Code" id="code" onkeyup=searchOnTable(3,'code')>
                </th>
                <th>
                    <input type="search" placeholder="Ref No." id="refNo" onkeyup=searchOnTable(4,'refNo')>
                </th>
                <th id="sl" colspan="2">
                    Create Date
                </th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchCodes}" var="searchCode">
                <tr>
                    <td>
                            ${searchCode.state}
                    </td>
                    <td>
                            ${searchCode.aType}
                    </td>
                    <td>
                            ${searchCode.dType}
                    </td>
                    <td>
                            ${searchCode.gCode}
                    </td>
                    <td>
                            ${searchCode.refNo}
                    </td>
                    <td>
                            ${searchCode.cDate}
                    </td>
                    <td>
                        Edit/Delete
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </form>
</div>
</table>
</body>
</html>
