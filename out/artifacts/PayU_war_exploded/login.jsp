<%--
  Created by IntelliJ IDEA.
  User: loveshgrover
  Date: 11/12/18
  Time: 3:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="inner">
    <form action="login" method="post">
        <table>
            <tr>
                <td><input type="email" name="emailAddress" placeholder="Enter Email Address"></td>
            </tr>
            <tr>
                <td><input type="password" name="passwordDetails" placeholder="Enter Password"></td>
            </tr>
            <tr>
                </br></br>
            </tr>
            <tr>
                <td><input type="submit" value="Log In"></td>
            </tr>
        </table>
        <p style="margin-left:-25%">${message}</p>
    </form>
</div>
</body>
</html>
