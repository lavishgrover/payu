<%--
  Created by IntelliJ IDEA.
  User: loveshgrover
  Date: 14/12/18
  Time: 1:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }
    function changeTab(id, unilink, link) {
        document.getElementById('iFrame').setAttribute('src', link);
        document.getElementById(id).style.background = "#cecece";
        document.getElementById(unilink).style.background = "000000";
    }
</script>
<html>
<head>
    <title>PayU</title>

</head>
<body style="margin: 20px;text-align: center">


<%
    //allow access only if session exists
    String user = null;
    if (session.getAttribute("user") == null) {
        response.sendRedirect("login");
    } else user = (String) session.getAttribute("user");
    String userName = null;
    String sessionID = null;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) userName = cookie.getValue();
            if (cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
        }
    }
%>

<div style="text-align:right;">
    <a href="logout">LogOut</a>
    <div style="margin-right: 50px">
        <h3 style="display:inline">Welcome </h3><%=userName%>
    </div>
</div>

<div style="margin-top: 20px">
    <button id="0"
            style="padding: 4px;background:#cecece;border-radius: 10px 10px 0 0;width: 40%; display:inline ;outline:0"
            onclick="changeTab(0,1,'createCode')">Create Code
    </button>
    <button id="1" style="padding: 4px;border-radius: 10px 10px 0 0;width: 40%; outline:0;"
            onclick="changeTab(1,0,'searchCode')">Search Code
    </button>

    <iframe id="iFrame" width="80%" frameborder="0" scrolling="no" onload=resizeIframe(this) src="createCode"
            style="height:auto;margin-top: 50px;margin-right: 10px;margin-left: 10px"></iframe>
</div>
</body>
</html>
