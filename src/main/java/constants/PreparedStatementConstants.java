package constants;


/**
 * Created by loveshgrover on 12/12/18.
 */
public class PreparedStatementConstants {

    public static final String GET_USER_COUNT =
            "SELECT COUNT(*) AS ROWCOUNT FROM " + MySqlTableConstants.USER_DETAILS_TABLE + " WHERE " +
                    MySqlTableConstants.USER_DETAILS_TABLE_EMAIL_COLUMN + "=? AND " +
                    MySqlTableConstants.USER_DETAILS_TABLE_PASSWORD_COLUMN + "=?;";


    public static final String UPDATE_USER_LAST_LOGIN =
            "UPDATE " + MySqlTableConstants.USER_DETAILS_TABLE + " SET " +
                    MySqlTableConstants.USER_DETAILS_TABLE_LAST_LOGIN_DATE_COLUMN + " = ?" + " WHERE " +
                    MySqlTableConstants.USER_DETAILS_TABLE_EMAIL_COLUMN + " = ?;";

    public static final String GET_STATES =
            "SELECT * FROM " + MySqlTableConstants.STATE_TABLE + ";";


    public static final String INSERT_CODE =
            "INSERT INTO " + MySqlTableConstants.CODE_TABLE + " VALUES (?,?,?,?,?,?);";

    public static final String GET_CODES = "SELECT * FROM " + MySqlTableConstants.CODE_TABLE + ";";

    public static final String GET_CODES_RANGE = "SELECT * FROM " + MySqlTableConstants.CODE_TABLE + " WHERE " +
            MySqlTableConstants.CODE_TABLE_CREATION_DATE_COLUMN + ">=? AND " +
            MySqlTableConstants.CODE_TABLE_CREATION_DATE_COLUMN + "<=?;";
}
