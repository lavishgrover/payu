package constants;

/**
 * Created by loveshgrover on 12/12/18.
 */
public class MySqlTableConstants {

    public static final String USER_DETAILS_TABLE = "UserDetails";
    public static final String USER_DETAILS_TABLE_EMAIL_COLUMN = "email";
    public static final String USER_DETAILS_TABLE_PASSWORD_COLUMN = "passwrd";
    public static final String USER_DETAILS_TABLE_LAST_LOGIN_DATE_COLUMN = "lastLoginDate";
    public static final String STATE_TABLE = "States";
    public static final String CODE_TABLE = "code";
    public static final String CODE_TABLE_CREATION_DATE_COLUMN = "cDate";
}
