package utils;

import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by loveshgrover on 12/12/18.
 */
public class MySqlUtils {
    @Autowired
    private PropertyLoader propertyLoader;


    public Connection getDatabaseConnectoin() throws ClassNotFoundException, SQLException {
        Properties properties = propertyLoader.getProperties();


        Class.forName(properties.getProperty("driverClassName"));
        final Connection con = DriverManager.getConnection(
                properties.getProperty("url"), properties.getProperty("username"), properties.getProperty("password"));
        return con;
    }
}
