package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by loveshgrover on 16/04/18.
 */
public class PropertyLoader {


    public final static String TAG = PropertyLoader.class.getName() + ":";

    private Properties prop = null;
    private InputStream input = null;

    public PropertyLoader() {
        prop = new Properties();
        try {
            input = PropertyLoader.class.getResourceAsStream("/config.properties");
            prop.load(input);
        } catch (IOException ex) {
            new Throwable(TAG + "No able to create instance");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    new Throwable(TAG + "InputStream Not close");
                }
            }
        }
    }

    public Properties getProperties() {
        return prop;
    }

}
