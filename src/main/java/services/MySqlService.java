package services;

import database.DatabaseBase;
import dto.SearchCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * Created by loveshgrover on 12/12/18.
 */
@Service
public class MySqlService {

    @Autowired
    @Qualifier("mySql")
    DatabaseBase databaseBase;

    public boolean insert(String state, String aType, String refNo, String dType, String code) {
        return databaseBase.insert(state, aType, refNo, dType, code);
    }

    public void delete() {
        databaseBase.delete();
    }

    public void update() {
        databaseBase.update();
    }

    public void userLoginAudit(String emailAddress) {
        databaseBase.userLoginAudit(emailAddress);
    }

    public int getUserCountByEmail(String emailAddress, String password) {
        return databaseBase.getUserCountByEmail(emailAddress, password);
    }

    public List<String> getStates() {
        return databaseBase.getStates();
    }
    public List<SearchCode> getSearchCode(String from,String to){
        return databaseBase.getSearchCode(from,to);
    }
}
