package servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.MySqlService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by loveshgrover on 11/12/18.
 */
@Component
public class Logout extends HttpServlet {

    @Autowired
    private MySqlService mySqlService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //invalidate the session if exists
        HttpSession session = req.getSession(false);
        Cookie userName = new Cookie("user", null);
        userName.setMaxAge(0);
        resp.addCookie(userName);
        if (session != null) {
            session.invalidate();
        }
        resp.sendRedirect("login.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //nothing
    }


}
