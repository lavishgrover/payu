package servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.MySqlService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by loveshgrover on 11/12/18.
 */
@Component
public class CreateCode extends HttpServlet {

    @Autowired
    private MySqlService mySqlService;

    static int uniqueCode = 123;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        mySqlService.getStates();
        req.setAttribute("states", mySqlService.getStates()); // Will be available as ${message}
        req.setAttribute("unique", uniqueCode++); // Will be available as ${message}
        req.getRequestDispatcher("createCode.jsp").forward(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        List<String> states = mySqlService.getStates();

        req.setAttribute("states", states);

        String state = req.getParameter("state");
        String aType = req.getParameter("aType");
        String refNo = req.getParameter("refNo");
        String dType = req.getParameter("dType");

        String errorMsgDropDownEmply = "";
        String errorMsgRefNo = "";
        String errorMsgDropDownInvalid = "";
        if (state == null || state.length() == 0 || "State*".equalsIgnoreCase(state)) {
            errorMsgDropDownEmply = "State";
        }
        if (aType == null || aType.length() == 0 || "A Type(C/S)*".equalsIgnoreCase(aType)) {
            if (errorMsgDropDownEmply.isEmpty()) {
                errorMsgDropDownEmply = "A Type";
            } else {
                errorMsgDropDownEmply += " & A Type";
            }
        }
        if (refNo == null || refNo.length() > 20) {
            errorMsgRefNo = "Ref No. is not in correct format.";
        }
        if (dType == null || dType.length() == 0 || "D Type(G/P/U)*".equalsIgnoreCase(dType)) {
            if (errorMsgDropDownEmply.isEmpty()) {
                errorMsgDropDownEmply = "D Type";
            } else {
                errorMsgDropDownEmply += " & D Type";
            }
        }
        if (!"State*".equalsIgnoreCase(state) && !states.contains(state)) {
            errorMsgDropDownInvalid = "State";
        }
        if (!"A Type(C/S)*".equalsIgnoreCase(aType) && !("C".equalsIgnoreCase(aType) || "S".equalsIgnoreCase(aType))) {
            if (errorMsgDropDownInvalid.isEmpty()) {
                errorMsgDropDownInvalid = "A Type";
            } else {
                errorMsgDropDownInvalid += " & A Type";
            }
        }
        if (!"D Type(G/P/U)*".equalsIgnoreCase(dType) && !("G".equalsIgnoreCase(dType) || "P".equalsIgnoreCase(dType) || "U".equalsIgnoreCase(dType))) {
            if (errorMsgDropDownInvalid.isEmpty()) {
                errorMsgDropDownInvalid = "D Type";
            } else {
                errorMsgDropDownInvalid += " & D Type";
            }
        }
        if (!errorMsgDropDownEmply.isEmpty() || !errorMsgRefNo.isEmpty() || !errorMsgDropDownInvalid.isEmpty()) {
            req.setAttribute("unique", uniqueCode++);
            req.setAttribute("errorDropDownEmpty", "<h3 style='text-align: center'>" + (errorMsgDropDownEmply.isEmpty() ? "" : errorMsgDropDownEmply + " is required.") + "</h3");
            req.setAttribute("errorDropDownInvalid", "<h3 style='text-align: center'>" + (errorMsgDropDownInvalid.isEmpty() ? "" : errorMsgDropDownInvalid + " is not in correct format.") + "</h3>");
            req.setAttribute("errorRefNo", "<h3 style='text-align: center'>" + errorMsgRefNo + "</h3>");
            req.getRequestDispatcher("createCode.jsp").forward(req, resp);
            return;
        }


        String code = state.substring(0, 2).toUpperCase() + aType + dType + Integer.toString(uniqueCode).substring(0, 3);//need to generate

        mySqlService.insert(state, aType, refNo, dType, code);
        req.setAttribute("unique", uniqueCode++);
        req.setAttribute("code", "<h3 style='text-align: center'>" + "Code has been created: " + "<span style='color:red;'>" + code + "</span>" + "</h3");
        req.getRequestDispatcher("createCode.jsp").forward(req, resp);


    }


}
