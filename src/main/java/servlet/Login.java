package servlet;

import constants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.MySqlService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by loveshgrover on 11/12/18.
 */
@Component
public class Login extends HttpServlet {

    @Autowired
    private MySqlService mySqlService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }


    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String emailAddress = req.getParameter("emailAddress");
        String passwordDetails = req.getParameter("passwordDetails");
        if ("POST".equalsIgnoreCase(req.getMethod()) && (emailAddress == null || emailAddress.length() == 0 || passwordDetails == null || passwordDetails.length() == 0)) {
            req.setAttribute("message", "<h1>Please enter your email and password</h1?"); // Will be available as ${message}
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }
        if ("POST".equalsIgnoreCase(req.getMethod())) {
            if (mySqlService.getUserCountByEmail(emailAddress, passwordDetails) == 0) {
                req.setAttribute("message", "Incorrect details, please enter correct details."); // Will be available as ${message}
                req.getRequestDispatcher("login.jsp").forward(req, resp);
            } else {
                mySqlService.userLoginAudit(emailAddress);
                HttpSession session = req.getSession();
                session.setAttribute("user", emailAddress);
                //setting session to expiry in 30 mins
                session.setMaxInactiveInterval(30 * 60);
                Cookie userName = new Cookie("user", emailAddress);
                userName.setMaxAge(30 * 60);
                resp.addCookie(userName);
                resp.sendRedirect("welcome.jsp");
            }
            return;
        }
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }


}
