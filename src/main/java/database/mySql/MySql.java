package database.mySql;

import constants.PreparedStatementConstants;
import database.DatabaseBase;
import dto.SearchCode;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import utils.MySqlUtils;

import javax.annotation.PostConstruct;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by loveshgrover on 12/12/18.
 */

@Repository("mySql")
public class MySql implements DatabaseBase {

    @Autowired
    MySqlUtils mySqlUtils;

    private Connection connection;

    SimpleDateFormat sdf =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @PostConstruct
    public void init() throws SQLException, ClassNotFoundException {
        connection = mySqlUtils.getDatabaseConnectoin();
    }

    public boolean insert(String state, String aType, String refNo, String dType, String code) {
        Date dt = new Date();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PreparedStatementConstants.INSERT_CODE);
            preparedStatement.setString(1, state);
            preparedStatement.setString(2, aType);
            preparedStatement.setString(3, dType);
            preparedStatement.setString(4, code);
            preparedStatement.setString(5, refNo);
            preparedStatement.setString(6, sdf.format(dt));
            return preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void delete() {
        System.out.println("Delete");

    }

    public void update() {
        System.out.println("Update");
    }

    public int getUserCountByEmail(String email, String password) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PreparedStatementConstants.GET_USER_COUNT);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int count = resultSet.getInt("rowcount");
            resultSet.close();

            return count;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void userLoginAudit(String email) {
        Date dt = new Date();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PreparedStatementConstants.UPDATE_USER_LAST_LOGIN);
            preparedStatement.setString(1, sdf.format(dt));
            preparedStatement.setString(2, email);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            //We can log here if user last logged in time is not updated
        }
    }

    public List<String> getStates() {
        List<String> states = new ArrayList<String>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PreparedStatementConstants.GET_STATES);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                states.add(resultSet.getString(2));
            }
            return states;
        } catch (SQLException e) {
            e.printStackTrace();
            //We can log here if user last logged in time is not updated
            return states;
        }
    }

    public List<SearchCode> getSearchCode(String from, String to) {
        List<SearchCode> searchCodes = new ArrayList<SearchCode>();
        try {
            PreparedStatement preparedStatement;

            if (from == null || from.isEmpty() || to == null || to.isEmpty()) {
                preparedStatement = connection.prepareStatement(PreparedStatementConstants.GET_CODES);
            } else {
                preparedStatement = connection.prepareStatement(PreparedStatementConstants.GET_CODES_RANGE);
                preparedStatement.setString(1, from);
                try {
                    preparedStatement.setString(2, sdf.format(DateUtils.addDays(DateUtils.parseDate(to, new String[]
                            {"yyyy-MM-dd"}), 1)));
                } catch (ParseException p) {
                    new ParseException(p.getMessage(), 1);
                }
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                SearchCode searchCode = new SearchCode();
                searchCode.setState(resultSet.getString(1));
                searchCode.setaType(resultSet.getString(2));
                searchCode.setdType(resultSet.getString(3));
                searchCode.setgCode(resultSet.getString(4));
                searchCode.setRefNo(resultSet.getString(5));
                searchCode.setcDate(resultSet.getDate(6).toString());
                searchCodes.add(searchCode);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //We can log here if user last logged in time is not updated
        }
        return searchCodes;
    }

}
