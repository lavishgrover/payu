package database;

import dto.SearchCode;

import java.text.ParseException;
import java.util.List;

/**
 * Created by loveshgrover on 11/12/18.
 */
public interface DatabaseBase {

    boolean insert(String state, String aType, String refNo, String dType, String code);

    void delete();

    void update();

    int getUserCountByEmail(String email, String password);


    void userLoginAudit(String email);

    List<String> getStates();

    List<SearchCode> getSearchCode(String from, String to);
}
